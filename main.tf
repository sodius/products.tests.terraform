provider "vsphere" {
  vsphere_server = var.vsphere_server
  user           = var.vsphere_user
  password       = var.vsphere_password

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = var.datacenter
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = var.network_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "centos" {
  name          = "${var.centos_name}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "learn" {
  count            = "${length(var.vm_app)}"
  name             = "${var.vm_name}-element(var.vm_app,${count.index + 1} ).${var.domain_name}"
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id

  num_cpus = 2
  memory   = 1024

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  wait_for_guest_net_timeout = -1
  wait_for_guest_ip_timeout  = -1

  disk {
    label            = "disk0"
    thin_provisioned = true
    size             = 100
  }

  guest_id = "centos7_64Guest"

  clone {
    template_uuid = data.vsphere_virtual_machine.centos.id
  }
}
/* Example for snapshprt
resource "vsphere_virtual_machine_snapshot" "learn" {
  virtual_machine_uuid = vsphere_virtual_machine.learn.id
  snapshot_name        = "first_snapshot"
  description          = "Created using Terraform"
  memory               = "true"
  quiesce              = "true"
  remove_children      = "false"
  consolidate          = "true"
}
*/

output "vm_ip" {
  value = vsphere_virtual_machine.learn[*].guest_ip_addresses
}
