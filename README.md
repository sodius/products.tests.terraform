# Learn Terraform vSphere

This repository allow us to create VM on VSphere.


# Command
to see what are the change: terraform plan

to apply these change: terraform apply

to destroy: terraform destroy

# Feature

Some feature need to be improve:

- configure DNS

- configure onepassword or backend for our variables

- make sure Ansible can recongize these vm

- allow ipV4
